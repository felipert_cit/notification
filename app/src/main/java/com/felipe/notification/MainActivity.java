package com.felipe.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final int NOTIFICATION_ID = 98;
    private Button btnDoit;
    private Button btnHard;


    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();

        btnDoit = (Button) findViewById(R.id.main_button_doit);
        btnHard = (Button) findViewById(R.id.main_button_hard);

        btnDoit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler mHandler = new Handler();
                mHandler.post(new DisplayNotification(getApplicationContext()));
            }
        });

        btnHard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildNotification();
                }
        });


    }

    protected void buildNotification() {

        Toast.makeText(context,"buildNotification",Toast.LENGTH_LONG);

        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(
                context,
                NOTIFICATION_ID,
                getIntent(),
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.mipmap.ic_launcher)
                        // Set Ticker Message
                .setTicker(context.getString(R.string.customnotificationticker))
                        // Dismiss Notification
                .setAutoCancel(true)
                        // Set PendingIntent into Notification
                .setContentIntent(pIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // build a complex notification, with buttons and such
            //
            builder = (NotificationCompat.Builder) builder.setContent(getComplexNotificationView());
        } else {
            // Build a simpler notification, without buttons
            //
            builder = (NotificationCompat.Builder) builder.setContentTitle(getTitle())
                    .setContentText("getText()")
                    .setSmallIcon(android.R.drawable.ic_menu_gallery);
        }


        Notification n;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            n = builder.build();
        } else {
            n = builder.getNotification();
        }

        //n.flags |= Notification.FLAG_NO_CLEAR;// | Notification.FLAG_ONGOING_EVENT;

        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, n);

        Toast.makeText(context, "done", Toast.LENGTH_LONG);

    }

    private RemoteViews getComplexNotificationView() {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews notificationView = new RemoteViews(
                context.getPackageName(),
                R.layout.notification_layout
        );

        // Locate and set the Image into customnotificationtext.xml ImageViews
       notificationView.setImageViewResource(
                R.id.imagenotileft,
                R.mipmap.ic_launcher);

        // Locate and set the Text into customnotificationtext.xml TextViews
        notificationView.setTextViewText(R.id.title, getTitle());
        notificationView.setTextViewText(R.id.text, "teste texto");


        return notificationView;
    }
}
